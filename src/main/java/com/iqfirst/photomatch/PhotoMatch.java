package com.iqfirst.photomatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotoMatch {
	public static void main(String[] args) {
		SpringApplication.run(PhotoMatch.class, args);
	}
}
