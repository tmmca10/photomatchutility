package com.iqfirst.photomatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class PhotoMatchController {
	@Autowired
	PhotoMatchService photoMatchService;
	
	@RequestMapping(value = "/photomatch", method = RequestMethod.POST)
	public ResponseEntity<?> matchPhoto(@RequestParam(value="custPhoto") MultipartFile custPhoto,
			@RequestParam(value="custPhotoId") MultipartFile custPhotoId){
		try {
			return new ResponseEntity<>(photoMatchService.matchPhoto(custPhoto, custPhotoId), HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/testservice", method = RequestMethod.POST)
	public ResponseEntity<?> matchPhoto(){
		return new ResponseEntity<>("hello", HttpStatus.OK);
	}

}
