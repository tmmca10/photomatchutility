package com.iqfirst.photomatch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PhotoMatchService {
	private static final Logger log = LoggerFactory.getLogger(PhotoMatchService.class);
	
	@Value("${images.base.dir}")
	String baseDirImages;
	
	@Value("${script.python.faceMatch}")
	String faceMatch;
	
	@Value("${command.python2}")
	String command2;
	
	@Value("${disallowed.file.types}")
	List<String> disallowedfileTypes;

	public ResponseEntity<?> matchPhoto(MultipartFile custPhoto, MultipartFile custPhotoId) throws IOException{
		
		String photoPath = saveFile(custPhoto,"Photo");
		String idPath = saveFile(custPhotoId, "PhotoId");
		if(match(photoPath, idPath)) {
			return new ResponseEntity<>("Matched",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("Not Matched",HttpStatus.OK);
		}
	}

	public boolean match(String photoPath, String idProofPath) throws IOException {
		ProcessBuilder builder = new ProcessBuilder("sh", "-c",
				command2 + " " + faceMatch + " " + photoPath + " " + idProofPath);
		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null) {
				break;
			} else if (line != null && line.trim().equalsIgnoreCase("[True]")) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	public String saveFile(MultipartFile file , String name) throws IOException {
		if(file != null && file.getName()!=null && !file.getName().isEmpty() && file.getSize()!=0) {
			String ext = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
			log.info("extension "+ext);
			if (ext != null && !ext.isEmpty()){
				for(String disallowedext : disallowedfileTypes) {
					if (ext.equalsIgnoreCase(disallowedext)) {
						log.info(ext + "files not allowed");
					}
				}
				String uploadDir = baseDirImages+"misc";
				Path fileStorageDir = Paths.get(uploadDir).toAbsolutePath().normalize();
				if (!Files.exists(fileStorageDir)) {
					fileStorageDir = Files.createDirectories(fileStorageDir);
					log.info("Directory created : "+fileStorageDir.toString());
				}
				
				Path filePath = fileStorageDir.resolve(name + "." + ext);
				log.info("file path : "+filePath.toString());
				Files.copy(file.getInputStream(), filePath , StandardCopyOption.REPLACE_EXISTING);
				return filePath.toString();
			}
		}
		return null;
	}

}
